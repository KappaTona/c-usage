#ifndef proejction_module_h
#define proejction_module_h
#include "geometry_types.h"


#define init_tesseract_data init_tesseract_data_values
#define init_tesseract_projection init_tesseract_projection_functions
#define cleanup_tesseract_projection cleanup_tesseract_projection_functions

typedef struct TesseractData
{
    double vertecies_3D[16][3];
    double vertecies_2D[16][2];
    MatrixOfDouble4D const view_matrix_4D;
    MatrixOfDouble3D const view_matrix_3D;
} TesseractData;

extern TesseractData init_tesseract_data();

typedef struct TesseractProjection
{
    void (*prepare_for_rendering)(TesseractData *data,
            double const some_time, double const some_time_scale);
} TesseractProjection;

extern TesseractProjection init_tesseract_projection();
extern void cleanup_tesseract_projection(TesseractProjection *data);


#endif //projection_helper_h
