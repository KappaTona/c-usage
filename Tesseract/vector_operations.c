#include "vector_operations.h"
#include <math.h>
#include <stdlib.h>


static VectorOfDouble3D inner_extract3D(PairOfVector3D const left_then_right);
static double inner_dot3D(PairOfVector3D const left_then_right);
static double inner_normalize3D(VectorOfDouble3D const v);
static VectorOfDouble3D inner_cross3D(PairOfVector3D const left_then_right);
static void inner_scale3D(VectorOfDouble3D *result, double const scale);

static VectorOfDouble4D inner_extract4D(PairOfVector4D const left_then_right);
static double inner_dot4D(PairOfVector4D const left_then_right);
static double inner_normalize4D(VectorOfDouble4D const v);
static VectorOfDouble4D inner_cross4D(TrioOfVector4D const left_then_right);
static void inner_scale4D(VectorOfDouble4D *result, double const scale);
 
VectorOperations3D init_vector_operations_3D()
{
    VectorOperations3D rv =
    {
        .extract=&inner_extract3D,
        .dot=&inner_dot3D,
        .normalize=&inner_normalize3D,
        .cross=&inner_cross3D,
        .scale=&inner_scale3D,
    };
    return rv;
}

void cleanup_vector_operations_3D(VectorOperations3D *function_set)
{
    if (function_set)
    {
        function_set->cross = NULL;
        function_set->dot = NULL;
        function_set->extract = NULL;
        function_set->normalize = NULL;
        function_set->scale = NULL;
        function_set = NULL;
    }
}

VectorOperations4D init_vector_operations_4D()
{
    VectorOperations4D rv =
    {
        .extract=&inner_extract4D,
        .dot=&inner_dot4D,
        .normalize=&inner_normalize4D,
        .cross=&inner_cross4D,
        .scale=&inner_scale4D,
    };
    return rv;
}

void cleanup_vector_operations_4D(VectorOperations4D *function_set)
{
    if (function_set)
    {
        function_set->cross = NULL;
        function_set->dot = NULL;
        function_set->extract = NULL;
        function_set->normalize = NULL;
        function_set->scale = NULL;
        function_set = NULL;
    }
}

static VectorOfDouble3D inner_extract3D(PairOfVector3D const left_then_right)
{
    VectorOfDouble3D left = left_then_right.first;
    VectorOfDouble3D right = left_then_right.second;

    return (VectorOfDouble3D)
    {
        left._0 - right._0,
        left._1 - right._1, 
        left._2 - right._2,
    };
}

static double inner_dot3D(PairOfVector3D const left_then_right)
{
    VectorOfDouble3D left = left_then_right.first;
    VectorOfDouble3D right = left_then_right.second;
    return left._0 * right._0 + left._1 * right._1 + left._2 * right._2;
}

static double inner_normalize3D(VectorOfDouble3D const v)
{
    return sqrt(inner_dot3D((PairOfVector3D){v, v}));
}

static VectorOfDouble3D inner_cross3D(PairOfVector3D const left_then_right)
{
    VectorOfDouble3D left = left_then_right.first;
    VectorOfDouble3D right = left_then_right.second;

    return (VectorOfDouble3D)
    {
        left._1 * right._2 - left._2 * right._1,
        left._2 * right._0 - left._0 * right._2,
        left._0 * right._1 - left._1 * right._0,
    };
}

static void inner_scale3D(VectorOfDouble3D *result, double const scale)
{
    result->_0 *= scale;
    result->_1 *= scale;
    result->_2 *= scale;
}

static VectorOfDouble4D inner_extract4D(PairOfVector4D const left_then_right)
{
    VectorOfDouble4D left = left_then_right.first;
    VectorOfDouble4D right = left_then_right.second;

    return (VectorOfDouble4D)
    {
        left._0 - right._0,
        left._1 - right._1, 
        left._2 - right._2,
        left._3 - right._3,
    };
}

static double inner_dot4D(PairOfVector4D const left_then_right)
{
    VectorOfDouble4D left = left_then_right.first;
    VectorOfDouble4D right = left_then_right.second;
    return left._0 * right._0 + left._1 * right._1 + left._2 * right._2 + left._3 * right._3;
}

static double inner_normalize4D(VectorOfDouble4D const v)
{
    return sqrt(inner_dot4D((PairOfVector4D){v, v}));
}

static VectorOfDouble4D inner_cross4D(TrioOfVector4D const first_second_third)
{
    VectorOfDouble4D u = first_second_third.first;
    VectorOfDouble4D v = first_second_third.second;
    VectorOfDouble4D w = first_second_third.third;

    double a = v._0 * w._1 - v._1 * w._0;
    double b = v._0 * w._2 - v._2 * w._0;
    double c = v._0 * w._3 - v._3 * w._0;
    double d = v._1 * w._2 - v._2 * w._1;
    double e = v._1 * w._3 - v._3 * w._1;
    double f = v._2 * w._3 - v._3 * w._2;

    return (VectorOfDouble4D)
    {
        ._0 = u._1 * f - u._2 * e + u._3*d,
        ._1 = -u._0 * f + u._2 * c - u._3 *b,
        ._2 = u._0 * e - u._1 * c + u._3 * a,
        ._3 = -u._0 * d + u._1 * b - u._2 * a
    };
}

static void inner_scale4D(VectorOfDouble4D *result, double const scale)
{
    result->_0 *= scale;
    result->_1 *= scale;
    result->_2 *= scale;
    result->_3 *= scale;
}
