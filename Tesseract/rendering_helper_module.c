#include "rendering_helper_module.h"
#include <ncurses.h>


static WINDOW *init_window(WindowBounds wb);
static bool inner_for_rendering(char projection_space[HEIGHT][WIDTH]);
static bool print_error_prepare_exit(WindowError const err);
static bool is_running = true;
static bool requesting_one_more_tesseract = false;
static bool inner_requesting_one_more_tesseract();

//TODO this is here, so I have 0 members in RenderingHelper
//also currently I work with one ncurses::WINDOW only.
static WINDOW *win = NULL;

RenderingHelper init_rendering_helper(WindowBounds wb)
{
    WINDOW *we = init_window(wb);
    if (!we)
    {
        is_running = print_error_prepare_exit((WindowError)
                {.msg="exiting from init_rendering_helper"});
        return (RenderingHelper){0};
    }

    return (RenderingHelper)
    {
        .for_rendering=&inner_for_rendering,
        .requested_one_more_tesseract=&inner_requesting_one_more_tesseract,
    };
}

static bool inner_requesting_one_more_tesseract()
{
    return requesting_one_more_tesseract;
}

static bool print_error_prepare_exit(WindowError const err)
{
    printf("--- RENDERING ERROR! ---\n");
    printf("msg:%s\nchar:%c\ni=%d\nj=%d\n"
            "RenderingHelper AND WINDOW ? %s\n",
            err.msg, err.position_character,err.y,err.x,
            win ? "WINDOW OK" : "WINDOW NULL");
    printf("---\n");
    is_running = false;
    return false;
}

static void inner_cleanup_window(WINDOW *win)
{
    if (win)
    {
        delwin(win);
        win = NULL;
        endwin();
    }
}

void cleanup_rendering_helper(RenderingHelper *function_set)
{
    if (function_set)
    {
        function_set->for_rendering = NULL;
        function_set->requested_one_more_tesseract= NULL;
        function_set = NULL;
    }
    inner_cleanup_window(win);
}

static WINDOW *init_window(WindowBounds wb)
{
    initscr();
    start_color();
    cbreak();
    noecho();
    curs_set(0);
    win = newwin(wb.y, wb.x, 0, 0);
    if (!win)
    {
        inner_cleanup_window(win);
        print_error_prepare_exit((WindowError)
                {.msg="ncurses::newwin alloc failed"});
        return NULL;
    }

    init_pair(1, COLOR_WHITE, COLOR_BLACK);
    wattron(win, COLOR_PAIR(1));
    keypad(win, 1);
    if (nodelay(win, 1) == ERR)
    {
        inner_cleanup_window(win);
        print_error_prepare_exit((WindowError)
                {.msg="ncurses::nodelay setup up returned ERR"});
        return NULL;
    }

    return win;
}

static bool is_window_ok()
{
    return win ? true : print_error_prepare_exit((WindowError)
        {.msg="WINDOW is NULL",});
}

static bool hard_interrupt(int key_up)
{
    return (key_up == KEY_UP) ? true : false;
}

static bool soft_interrupt()
{
    if (!is_window_ok())
        return false;

    if (nodelay(win, 0) == ERR)
    {
        return print_error_prepare_exit((WindowError)
            {.msg = "ncurses::nodelay turn off failed",});
    }

    int interrupt = wgetch(win);
    if (interrupt == ERR)
    {
        return print_error_prepare_exit((WindowError)
            {.msg = "ncurses::wgetch failed",});
    }
    if (nodelay(win, 1) == ERR)
    {
        return print_error_prepare_exit((WindowError)
            { .msg = "ncurses::nodelay turn on failed" });
    }

    return hard_interrupt(interrupt) ? false : true;
}

static bool one_more_tesseract()
{
    requesting_one_more_tesseract = true;
    return true;
}

static bool one_less_tesseract()
{
    requesting_one_more_tesseract = false;
    return true;
}

static bool listening()
{
    if (!is_window_ok())
        return false;

    int character_listening = wgetch(win);
    if (character_listening == ERR)
        return true;

    if (character_listening == KEY_RIGHT)
        return one_more_tesseract();

    if (character_listening == KEY_LEFT)
        return one_less_tesseract();

    if (hard_interrupt(character_listening))
        return false;

    return soft_interrupt();
}

static bool inner_for_rendering(char projection_space[HEIGHT][WIDTH])
{
    if (!is_running)
        return false;

    if (!is_window_ok())
        return false;

    for (int i = 0; i < HEIGHT; ++i)
    {
        for (int j = 0; j < WIDTH; ++j)
        {
            if (mvwaddch(win, i, j, projection_space[i][j]) == ERR)
            {
                WindowError rv = (WindowError)
                {
                    .position_character=projection_space[i][j],
                    .y=i, .x=j, .msg="ncurses::mvwaddch returned ERR"
                };
                return print_error_prepare_exit(rv);
            }
        }
    }
    wrefresh(win);
    return listening();
}
