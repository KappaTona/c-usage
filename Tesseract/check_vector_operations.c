#include "vector_operations.h"
#include <check.h>
#include <stdlib.h>


static double const tol = 0.0003;
START_TEST(test_extract3D)
{
    VectorOperations3D ops = init_vector_operations_3D();
    VectorOfDouble3D rv = ops.extract((PairOfVector3D){{8,2,3}, {1,2,3}});

    ck_assert_double_eq_tol(7, rv._0, tol);
    ck_assert_double_eq_tol(0, rv._1, tol);
    ck_assert_double_eq_tol(0, rv._2, tol);
    cleanup_vector_operations_3D(&ops);
}
END_TEST

START_TEST(test_dot3D)
{
    VectorOperations3D ops = init_vector_operations_3D();
    double expected = 0., expected2=14.;
    PairOfVector3D left_then_right = {0};
    PairOfVector3D left_then_right2 = {{1.,2.,3.,}, {1.,2.,3.,}};

    double dot_expected = ops.dot(left_then_right);
    ck_assert_double_eq(expected, dot_expected);

    double dot_expected2 = ops.dot(left_then_right2);
    ck_assert_double_eq(expected2, dot_expected2);
    cleanup_vector_operations_3D(&ops);
}
END_TEST

START_TEST(test_normalize3D)
{
    VectorOperations3D ops = init_vector_operations_3D();
    VectorOfDouble3D left = {0}, left2 = {1.,2.,3.};
    double expected = 0., expected2 = sqrt(14);

    double normalize_left = ops.normalize(left);
    double normalize_left2 = ops.normalize(left2);

    ck_assert_double_eq(expected, normalize_left);
    ck_assert_double_eq(expected2, normalize_left2);
    cleanup_vector_operations_3D(&ops);
}
END_TEST

START_TEST(test_cross3D)
{
    VectorOperations3D ops = init_vector_operations_3D();
    PairOfVector3D u_then_v = (PairOfVector3D){
    {1.,2.,3.,}, {5.,6.,7.,}};
    VectorOfDouble3D result = ops.cross(u_then_v);

    u_then_v.first = (VectorOfDouble3D){1., -2., 3.,};
    u_then_v.second = (VectorOfDouble3D){5 *- 2, 6 *- 2, 7 *- 2};
    VectorOfDouble3D result2 = ops.cross(u_then_v);

    ck_assert_double_eq(result._0, -4.);
    ck_assert_double_eq(result._1, 8.);
    ck_assert_double_eq(result._2, -4.);
    ck_assert_double_eq(result2._0, 64.);
    ck_assert_double_eq(result2._1, -16.);
    ck_assert_double_eq(result2._2, -32.);
    cleanup_vector_operations_3D(&ops);
}
END_TEST

START_TEST(test_scale3D)
{
    VectorOperations3D ops = init_vector_operations_3D();
    VectorOfDouble3D result = {1.,1.,1.,};
    double s = 3.;

    ops.scale(&result, s);
    ck_assert_double_eq(result._0, 3.);
    ck_assert_double_eq(result._1, 3.);
    ck_assert_double_eq(result._2, 3.);
    cleanup_vector_operations_3D(&ops);
}
END_TEST

START_TEST(test_dot4D)
{
    VectorOperations4D ops = init_vector_operations_4D();
    PairOfVector4D left_then_right = {0};
    PairOfVector4D left_then_right2 = { {1.,2.,3.,4.}, {1.,2.,3.,4.} };
    double expected = 0., expected2=30.;

    double result = ops.dot(left_then_right);
    double result2 = ops.dot(left_then_right2);
    ck_assert_double_eq(expected, result);
    ck_assert_double_eq(expected2, result2);
    cleanup_vector_operations_4D(&ops);
}
END_TEST

START_TEST(test_normalize4D)
{
    VectorOperations4D ops = init_vector_operations_4D();
    VectorOfDouble4D left = {0};
    VectorOfDouble4D left2 = {1.,2.,3.,4.};
    double expected = 0., expected2 = sqrt(30);
    double result = ops.normalize(left);
    double result2 = ops.normalize(left2);
;

    ck_assert_double_eq(expected, result);
    ck_assert_double_eq(expected2, result2);
    cleanup_vector_operations_4D(&ops);
}
END_TEST

START_TEST(test_cross4D)
{
    VectorOperations4D ops = init_vector_operations_4D();
    TrioOfVector4D u_v_w = { {1.,2.,3.,4},{5.,6.,7.,8.}, {9., 10., 11, 12}};

    VectorOfDouble4D result = ops.cross(u_v_w);
    ck_assert_double_eq(result._0, 0.);
    ck_assert_double_eq(result._1, 0.);
    ck_assert_double_eq(result._2, 0.);
    ck_assert_double_eq(result._3, 0.);

    u_v_w = (TrioOfVector4D){
        {1., -2., 3., -4.}, {5 *- 2, 6 *- 2, 7 *- 2, 8 *- 2}, {9., 10., 11, 12} };

    VectorOfDouble4D result2 = ops.cross(u_v_w);
    ck_assert_double_eq(result2._0, -96.);
    ck_assert_double_eq(result2._1, 128.);
    ck_assert_double_eq(result2._2, 32.);
    ck_assert_double_eq(result2._3, -64.);
    cleanup_vector_operations_4D(&ops);
}
END_TEST

START_TEST(test_extract4D)
{
    VectorOperations4D ops = init_vector_operations_4D();
    PairOfVector4D left_then_right = {{4., 6., 7., 9.}, {11., 33., 44., 6.}};
    VectorOfDouble4D result = ops.extract(left_then_right);
    VectorOfDouble4D expected = {-7, -27, -37, 3};

    ck_assert_double_eq(result._0, expected._0);
    ck_assert_double_eq(result._1, expected._1);
    ck_assert_double_eq(result._2, expected._2);
    ck_assert_double_eq(result._3, expected._3);
    cleanup_vector_operations_4D(&ops);
}
END_TEST

START_TEST(test_scale4D)
{
    VectorOperations4D ops = init_vector_operations_4D();
    VectorOfDouble4D result = {1.,1.,1.,2.};
    double s = 3.;
    ops.scale(&result, s);

    ck_assert_double_eq(result._0, 3.);
    ck_assert_double_eq(result._1, 3.);
    ck_assert_double_eq(result._2, 3.);
    ck_assert_double_eq(result._3, 6.);
    cleanup_vector_operations_4D(&ops);
}
END_TEST


Suite *vector_operations_suite()
{
    Suite *s = suite_create("vector_operations_suite");
    TCase *tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_extract3D);
    tcase_add_test(tc_core, test_dot3D);
    tcase_add_test(tc_core, test_normalize3D);
    tcase_add_test(tc_core, test_cross3D);
    tcase_add_test(tc_core, test_scale3D);
    tcase_add_test(tc_core, test_extract4D);
    tcase_add_test(tc_core, test_dot4D);
    tcase_add_test(tc_core, test_normalize4D);
    tcase_add_test(tc_core, test_cross4D);
    tcase_add_test(tc_core, test_scale4D);
     

    suite_add_tcase(s, tc_core);
    return s;
}

int main()
{
    Suite *s = vector_operations_suite();
    SRunner *sr = srunner_create(s);

    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);

    srunner_free(sr);
    return (number_failed ? EXIT_FAILURE : EXIT_SUCCESS);

}
