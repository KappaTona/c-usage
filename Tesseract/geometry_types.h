#ifndef geometry_types_h
#define geometry_types_h

#define PI 3.141592653589793
#define HEIGHT 25
#define WIDTH 50

typedef struct VectorOfDouble4D {
    double _0, _1, _2, _3;
} VectorOfDouble4D;

typedef struct VectorOfDouble3D {
    double _0, _1, _2;
} VectorOfDouble3D;

typedef struct MatrixOfDouble3D
{
    VectorOfDouble3D  col_0, col_1, col_2;
} MatrixOfDouble3D;

typedef struct MatrixOfDouble4D 
{
    VectorOfDouble4D col_0, col_1, col_2, col_3;
} MatrixOfDouble4D;


#endif //geometry_types_h
